import 'dart:convert';
import 'dart:io';

import 'package:test/test.dart';

main() {
  List<List<String>> buildContent(String s, {int nbJours = -1}) {
    LineSplitter ls = LineSplitter();
    // Output contient chaque ligne de l'output
    List<String> output = ls.convert(s);
    // Output per day contient la liste des ligne pour chaque jour ( pour 5 jours, il contiendra 5 list de string pour chacun des jours)
    List<List<String>> outputPerDay = <List<String>>[];
    // Buffer temporaire pour la copie
    List<String> buffer = <String>[];
    int nbItterations = 0;
    for (String s in output) {
      //Le nombre de jours permet de ne pas charger tout le fichier de référence lorsqu'on en a pas besoin
      if (nbJours == -1 || nbJours > nbItterations) {
        if (s == "") {
          outputPerDay.add(List<String>.from(buffer));
          buffer.clear();
          nbItterations++;
          //A partir d'ici on ne prends plus en compte la première ligne dont on a plus besoin
        } else if (s != "OMGHAI!") {
          buffer.add(s);
        }
      }
    }
    return outputPerDay;
  }

  test("Test", () async {
    print(
        "Entrer le nombre de jour à tester (0 ou rien pour le nombre par défaut)");
    String count = stdin.readLineSync();
    if (count == null || count == "0" || count == "") {
      count = "2";
    }

    var process = await Process.run("dart", ["bin/main.dart"]);

    // On s'attend a ce qu'il n'y ai pas d'erreur
    expect(process.stderr, "");

    //Récupération des données de l'éxecution actuelle
    List<List<String>> outputPerDay = buildContent(process.stdout);

    //Récupération des données du fichier de référence
    File file = File("test/reference/reference.txt");
    String s = file.readAsStringSync();
    List<List<String>> referencePerDay = buildContent(s, nbJours: 2);

    //Tests
    expect(true, referencePerDay.length == outputPerDay.length);
    for (int i = 0; i > referencePerDay.length; i++) {
      expect(
          true,
          referencePerDay.elementAt(i)?.length ==
              outputPerDay.elementAt(i)?.length);
      for (int j = 0; j > referencePerDay.elementAt(i)?.length; j++) {
        expect(
            true,
            referencePerDay.elementAt(i)?.elementAt(j) ==
                outputPerDay.elementAt(i)?.elementAt(j));
      }
    }
  });
}
